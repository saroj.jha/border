import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {CountryserviceService} from 'src/app/countryservice.service';
import{Router,} from '@angular/router';



export class Country{

  constructor(
    
     
     Countrys:any,
  ) {
  }

}

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {


  Countrys: Country[] = [];
  country=[];
  name:any;
  Country:any;
  constructor(private httpClinet : HttpClient, 
              private restservice : CountryserviceService,
              private route: Router,
            
              ){  }

  ngOnInit(): void {

    this.getCountrys();

  }
    getCountrys(){ 


    this.restservice.getCountryData().subscribe(
        response => {
          console.log(response);
          this.Country = response;
          this.Countrys=this.Country.list;

        }
      );
      }

       search(){
         if(this.name ==""){
           this.ngOnInit();
         }else{
          this.Country =this.Country.filter((res: { name: string; }) =>{
            return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
          });
        
      }
    }

   
}

  
 
  


